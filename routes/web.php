<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\InicioController;
use App\Http\Controllers\RecursosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [InicioController::class,'home'])->name('home');

Route::get('recursos', [RecursosController::class,'index'])->name('recursos.index');

Route::get('recursos/crear', [RecursosController::class,'create'])->name('recursos.create')->middleware('auth');

Route::get('recursos/{recurso}', [RecursosController::class,'show'])->name('recursos.show');

Route::put('recursos/{recurso}', [RecursosController::class,'update'])->name('recursos.update')->middleware('auth');

Route::post('recursos/store', [RecursosController::class,'store'])->name('recursos.store')->middleware('auth');

Route::get('recursos/{recurso}/editar', [RecursosController::class,'edit'])->name('recursos.edit')->middleware('auth');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
