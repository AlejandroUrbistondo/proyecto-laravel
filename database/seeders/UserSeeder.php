<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = new User();
    	$user1->name = 'Alex';
        $user1->email = 'alex@alex.com';
        $user1->password = bcrypt('alex');
    	$user1->save();
        $user2 = new User();
    	$user2->name = 'Urbis';
        $user2->email = 'urbis@urbis.com';
        $user2->password = bcrypt('urbis');
    	$user2->save();
    }
}
