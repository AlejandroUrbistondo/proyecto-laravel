<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\TipoRecursos;

class TipoRecursosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$tiporecursos1 = new TipoRecursos();
    	$tiporecursos1->tipo = 'Script';
    	$tiporecursos1->save();
    	$tiporecursos2 = new TipoRecursos();
    	$tiporecursos2->tipo = 'Modelo 3D';
    	$tiporecursos2->save();
    }
}
