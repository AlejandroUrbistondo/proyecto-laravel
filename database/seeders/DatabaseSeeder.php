<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        DB::table('recursos')->delete();
        DB::table('tipo_recursos')->delete();
        DB::table('users')->delete();
        $this->call(UserSeeder::class);
        $this->call(TipoRecursosSeeder::class);
        $this->call(RecursosSeeder::class);
    }
}
