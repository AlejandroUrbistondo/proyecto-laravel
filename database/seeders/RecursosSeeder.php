<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Recurso;

class RecursosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $recurso1 = new Recurso();
    	$recurso1->users_id = 1;
        $recurso1->nombre_recurso = 'Script de prueba';
        $recurso1->ruta = 'Avatar_Akaji.png';
        $recurso1->tipo_recursos_id = 1;
    	$recurso1->save();
    	$recurso2 = new Recurso();
    	$recurso2->users_id = 2;
        $recurso2->nombre_recurso = 'Modelo 3D de prueba';
        $recurso2->ruta = 'Avatar_Koa_1.png';
        $recurso2->tipo_recursos_id = 2;
    	$recurso2->save();
    }
}
