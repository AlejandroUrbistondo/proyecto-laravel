<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recurso extends Model
{
    use HasFactory;

    protected $table = 'recursos';

    public function usuario()
    {
    	return $this->belongsTo(User::class, 'users_id');
    }

    public function tipoRecurso()
    {
    	return $this->belongsTo(TipoRecursos::class, 'tipo_recursos_id');
    }
}
