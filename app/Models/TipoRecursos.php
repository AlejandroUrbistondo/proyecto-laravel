<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;



class TipoRecursos extends Model
{
    use HasFactory;

    protected $table = 'tipo_recursos';

    public function recursos()
    {
    	return $this->hasMany(Recurso::class);
    }
}
