<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Recurso;
use App\Models\TipoRecursos;
use Illuminate\Support\Facades\Auth;

class RecursosController extends Controller
{
    public function index()
    {
    	$recursos = Recurso::all();
        return view('recursos.index', compact('recursos'));
    }

    public function show(Recurso $recurso)
    {
        return view('recursos.show', compact('recurso'));
    }

    public function create()
    {
        $tipos = TipoRecursos::All();
        return view('recursos.create', compact('tipos'));
    }

    public function edit(Recurso $recurso)
    {
        $tipos = TipoRecursos::All();
        return view('recursos.edit')->with(compact('recurso', 'tipos'));
    }

    public function update(Request $request, Recurso $recurso)
    {
        $recurso->nombre_recurso = $request->input('nombre_recurso');
        $recurso->tipo_recursos_id = $request->input('tipo_recursos_id');

        $recurso->save();

        $tipos = TipoRecursos::All();
        return view('recursos.edit', compact('recurso', 'tipos'));
    }

    public function store(Request $request)
    {
        $recurso = new Recurso();

        $recurso->users_id = Auth::id();
        $recurso->nombre_recurso = $request->input('nombre_recurso');
        $recurso->tipo_recursos_id = $request->input('tipo_recursos_id');

        $request->validate([
            'archivo_recurso' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $custom_file_name = $request->file('archivo_recurso')->getClientOriginalName();
        $path = $request->file('archivo_recurso')->storeAs('/public/recursos', $custom_file_name);
        
        $recurso->ruta = $custom_file_name;

        $recurso->save();

        $tipos = TipoRecursos::All();
        return view('recursos.create', compact('tipos'));
    }
}