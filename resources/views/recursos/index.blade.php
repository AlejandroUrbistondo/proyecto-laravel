@extends('layouts.master') 
 
@section('titulo')
  Repositorios
@endsection 
 
@section('contenido')
  <div class="row">
    @foreach( $recursos as $recurso )
      <div class="col-xs-12 col-sm-6 col-md-4 md-6">
        <a href="{{ route('recursos.show', $recurso) }}">
          <h5>{{ $recurso->nombre_recurso }}</h5>
        </a>
        <label>Tipo: {{ $recurso->tipoRecurso->tipo }}</label>
      </div>
    @endforeach
  </div>
@endsection 