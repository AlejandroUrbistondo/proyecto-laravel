@extends('layouts.master') 
 
@section('titulo')
  Recurso
@endsection 
 
@section('contenido')
<div class="row">
  <div class="col-sm-3">      
    <img src="{{ $recurso->imagen }}" style="height:200px"/>
  </div>
  <div class="col-sm-9">
    <h2>{{ $recurso->nombre_recurso }}</h2>
    <h4>Propietario: </h4>
    <p>{{ $recurso->usuario->name }}</p>
    <h4>Tipo de Recurso: </h4>
    <p>{{ $recurso->tipoRecurso->tipo }}</p>
    <h4>Imagen guardada</h4>
    <img src="{{ asset('/storage/recursos/'.$recurso->ruta) }}" class="img-fluid"/>
    <br>
    <br>

    @if (Auth::id() == $recurso->usuario->id)
      <a class="btn btn-primary" href="{{ route('recursos.edit', $recurso) }}" role="button">Editar</a>
    @endif
    <a class="btn btn-outline-secondary" href="{{ route('recursos.index') }}" role="button">Volver al listado</a>
  </div>
</div> 
@endsection 