@extends('layouts.master') 
 
@section('titulo')
  Crear Recurso
@endsection 
 
@section('contenido')
<div class="row">
  <div class="offset-md-3 col-md-6">
    <div class="card">
      <div class="card-header text-center">
        Crear Recurso
      </div>
      <div class="card-body" style="padding:30px">        
        <form method="POST" action="{{ route('recursos.store') }}" enctype="multipart/form-data">
        @csrf
          <div class="form-group">
            <label for="nombre_recurso">Nombre del recurso</label>
            <input type="text" name="nombre_recurso" id="nombre_recurso" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="tipo_recursos_id">Tipo de Recurso</label>
            <br>
            <select name="tipo_recursos_id" id="tipo_recursos_id">              
              @foreach ($tipos as $tipo)
                  <option value="{{$tipo->id}}">{{$tipo->tipo}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="archivo_recurso">Archivo a subir</label>
            <input type="file" name="archivo_recurso" id="archivo_recurso" class="form-control-file" required>
          </div>
          <div class="form-group text-center">
            <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Crear recurso</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection 